﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nganhang.Controllers
{
    public class HomeController : Controller
    {
        public const string PATH = "~/Views/Home/";
        public ActionResult Index()
        {
            return View(PATH + "Index.cshtml");
        }
        public ActionResult VayVonBangLuong()
        {
            return View(PATH + "VayVonBangLuong.cshtml");
        }

        //public ActionResult Contact()
        //{
        //    return View();
        //}
    }
}