﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nganhang.Startup))]
namespace Nganhang
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
